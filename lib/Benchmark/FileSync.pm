package Benchmark::FileSync;
use strict;
use warnings;
use Carp 'croak';
use Time::HiRes qw(time sleep);
use File::Temp;
use File::Copy;
use File::Path;
use Digest::MD5;
use List::Util qw(max sum);
use Fcntl qw(O_WRONLY O_CREAT O_TRUNC);
use lib File::Spec->catdir(__FILE__, File::Spec->updir, 'lib');
use Test::Process;

sub _get_dir {
    my (undef, $test_dir) = File::Spec->splitpath(__FILE__);
    my $updir = File::Spec->updir;
    my $dir = File::Spec->catdir(__FILE__, ($updir) x 3);
    Cwd::abs_path($dir);
}

my $dir = _get_dir() or die 'no dir';

my $server_script_fn = File::Spec->catdir($dir, 'server.pl');
my $client_script_fn = File::Spec->catdir($dir, 'client.pl');

my $port = 8888;
my $localhost = '127.0.0.1';
my $wait_interval_sec = 0.001;

my @fields = qw(
    initial_file_size
    nr_clients_synced
    nr_clients_fresh
    append_chunk_size
    nr_appends
    repeat
);

sub new {
    my ($class, %params) = @_;
    my $self = {};

    for my $field (@fields) {
        $self->{$field} = $params{$field} // croak "no $field";
    }

    bless $self => $class;
    return $self;
}

sub run {
    my ($self) = @_;
    for (1..$self->{repeat}) {
        $self->_run_iter;
        $self->_cleanup;
    }
}

sub pretty_print_stats {
    my ($self) = @_;
    my $stats = $self->_stats;
    for my $f (@fields) {
        printf "%-20s: %d\n", $f, $self->{$f};
    }
    for my $k (sort keys %$stats) {
        my $val = $stats->{$k};
        my $val_str = defined($val) ? sprintf('%.5f', $val) : '-';
        printf "%-20s: %s\n", $k, $val_str;
    }
    print "\n";
}

sub _stats {
    my ($self) = @_;
    return {
        chunk_sync_time_avg => avg($self->{synced_clients_t}),
        chunk_sync_time_max => max(@{$self->{synced_clients_t}}),
        full_sync_time_avg  => avg($self->{fresh_clients_t}),
        full_sync_time_max  => max(@{$self->{fresh_clients_t}}),
    };
}

sub avg {
    my ($list) = @_;
    return undef unless @$list;
    return sum(@$list) / @$list;
}

sub _run_iter {
    my ($self) = @_;

    $self->_create_server;
    $self->_create_synced_clients;
    $self->_create_fresh_clients;

    $self->_create_server_file;
    $self->_append_to_server_file($self->{initial_file_size});
    $self->_copy_synced_files;
    $self->_start_server;
    sleep 0.5;
    $self->_start_fresh_clients;
    $self->_start_synced_clients;

    my $synced_ts = time;

    $self->{synced_clients_t} = [];
    $self->{fresh_clients_t}  = [];

    for (1..$self->{nr_appends}) {
        $self->_append_to_server_file($self->{append_chunk_size});
        my $fresh_ts = time;
        $self->_wait_for_synced_clients;
        push @{$self->{synced_clients_t}}, time - $fresh_ts;
    }

    $self->_wait_for_fresh_clients;
    push @{$self->{fresh_clients_t}}, time - $synced_ts;

    $self->_stop_all_processes;
    $self->_validate_files;
}

sub _create_server {
    my ($self) = @_;
    $self->{server} = {
        file => File::Spec->catdir($self->_tmp_dir, 'server_file'),
    };
}

sub _create_synced_clients {
    my ($self) = @_;
    $self->{clients_synced} = [];
    for my $n (1..$self->{nr_clients_synced}) {
        push @{$self->{clients_synced}}, {
            file => File::Spec->catdir($self->_tmp_dir, "synced_client_file_$n"),
        };
    }
}

sub _create_fresh_clients {
    my ($self) = @_;
    $self->{clients_fresh} = [];
    for my $n (1..$self->{nr_clients_fresh}) {
        push @{$self->{clients_fresh}}, {
            file => File::Spec->catdir($self->_tmp_dir, "fresh_client_file_$n"),
        };
    }
}

sub _create_server_file {
    my ($self) = @_;
    my $fn = $self->{server}{file};
    sysopen my $fh,  $fn, O_WRONLY | O_CREAT | O_TRUNC or die $!;
}

# TODO: speed up
sub _append_to_server_file {
    my ($self, $size) = @_;
    return unless $size;
    my $fn = $self->{server}{file};
    open my $fh, '>>:raw:bytes', $fn or die "$fn: $!";
    while ($size > 0) {
        my $s = 1024 * 1024;
        $s = $size if $s > $size;
        my $bytes = _rand_bytes($s);
        $fh->print($bytes) or die "failed to fill $fn";
        $size -= $s;
    }
}

sub _copy_synced_files {
    my ($self) = @_;
    my $server_file = $self->{server}{file};
    for my $c (@{$self->{clients_synced}}) {
        copy $server_file => $c->{file} or die "copy: $!";
    }
}

sub _start_server {
    my ($self) = @_;
    my $server = $self->{server};
    my $cmd = join ' ',
        'perl',
        $server_script_fn,
        "--file=$server->{file}",
        "--port=$port";
    my $p = Test::Process->new(
        'perl',
        $server_script_fn,
        "--file=$server->{file}",
        "--port=$port",
    );
    $p->is_alive or die "server is not alive";
    $server->{process} = $p;
}

sub _start_fresh_clients {
    my ($self) = @_;
    for my $c (@{$self->{clients_fresh}}) {
        my $p = Test::Process->new(
            'perl',
            $client_script_fn,
            "--file=$c->{file}",
            "--host=$localhost",
            "--port=$port",
        );
        $c->{process} = $p;
    }
}

sub _start_synced_clients {
    my ($self) = @_;
    for my $c (@{$self->{clients_synced}}) {
        my $p = Test::Process->new(
            'perl',
            $client_script_fn,
            "--file=$c->{file}",
            "--host=$localhost",
            "--port=$port",
        );
        $c->{process} = $p;
    }
}

sub _wait_for_fresh_clients {
    my ($self) = @_;
    my $server_file_size = (stat $self->{server}{file})[7];
    for my $c (@{$self->{clients_fresh}}) {
        while (1) {
            my $size = (stat $c->{file})[7];
            last if ($size // 0) == $server_file_size;
            sleep $wait_interval_sec;
        }
    }
}

sub _wait_for_synced_clients {
    my ($self) = @_;
    my $server_file_size = (stat $self->{server}{file})[7];
    for my $c (@{$self->{clients_synced}}) {
        while (1) {
            my $size = (stat $c->{file})[7];
            last if $size == $server_file_size;
            sleep $wait_interval_sec;
        }
    }
}

sub _stop_all_processes {
    my ($self) = @_;

    my @p = (
      @{$self->{clients_synced}},
      @{$self->{clients_fresh}},
      $self->{server},
    );

    for my $p (@p) {
      next unless $p->{process};
      $p->{process}->kill;
      1 while $p->{process}->is_alive;
    }
}

sub _validate_files {
    my ($self) = @_;

    my $expected_file_size =
        $self->{initial_file_size} +
        $self->{append_chunk_size} * $self->{nr_appends};


    my $server         = $self->{server};
    my $clients_synced = $self->{clients_synced};
    my $clients_fresh  = $self->{clients_fresh};
    my $prev_md5_sum;

    for my $p ($server, @$clients_synced, @$clients_fresh) {
        my $file = $p->{file};
        open my $fh, '<:raw:bytes', $file or die "$file: $!";

        my $file_size = (stat $fh)[7];
        my $md5_sum   = Digest::MD5->new->addfile($fh)->hexdigest;

        $prev_md5_sum //= $md5_sum;

        die "$file: file_size != expected_file_size"
            if $file_size != $expected_file_size;
        die "md5 sums don't match"
            if $md5_sum ne $prev_md5_sum;
    }
}

sub _cleanup {
    my ($self) = @_;
    File::Path::remove_tree($self->{tmp_dir});
    delete $self->{tmp_dir};
}

sub _tmp_dir {
    my ($self) = @_;
    $self->{tmp_dir} //= File::Temp::tempdir(
        'filesync-benchmark-XXXXX',
        TMPDIR => 1,
        CLEANUP => 1,
    );
    $self->{tmp_dir};
}

sub _rand_bytes {
    my ($len) = @_;
    return join '', map chr(rand 256), 1..$len;
}

1;
