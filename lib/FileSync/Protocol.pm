package FileSync::Protocol;
use strict;
use warnings;
use Exporter 'import';
use Carp 'croak';

our @EXPORT_OK = qw(
    HEADER_SIZE
    BEGIN_T
    CHUNK_T
    ERR_OFFSET_TOO_BIG_T
    ERR_UNKNOWN_T
    pack_message
    unpack_header
    unpack_chunk
);

use constant {
    HEADER_SIZE => 5,
    BEGIN_T => 0,
    CHUNK_T => 1,
    ERR_OFFSET_TOO_BIG_T => 2,
    ERR_UNKNOWN_T => 255,
};

my %types = map { $_ => 1 } (
    BEGIN_T,
    CHUNK_T,
    ERR_OFFSET_TOO_BIG_T,
    ERR_UNKNOWN_T,
);

sub pack_message {
    my (%param) = @_;
    my $type = $param{type} // croak 'no type';
    my $size = $param{size} // croak 'no size';
    croak 'invalid type' unless $types{$type};
    croak 'invalid size' unless $size >= 0;
    if ($type eq CHUNK_T) {
        my $chunk = $param{chunk} // croak 'no chunk';
        # TODO: no need to pass size if we pass chunk
        croak 'invalid size' if $size != length($chunk);
        return pack "CNa$size", $type, $size, $chunk;
    }
    else {
        return pack 'CN', $type, $size;
    }
}

sub unpack_header {
    my ($header) = @_;
    croak 'no header'      unless defined $header;
    croak 'invalid header' unless length($header) == HEADER_SIZE;

    my ($type, $len) = unpack 'CN', $header;
    croak 'invalid type'   unless $types{$type};
    croak 'invalid length' unless $len >= 0;

    return {
        type   => $type,
        length => $len
    };
}

sub unpack_chunk {
    my ($chunk) = @_;
    croak 'no chunk' unless defined $chunk;

    my $len = length($chunk);
    return unpack "a$len", $chunk;
}

1;
