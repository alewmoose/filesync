package FileSync::File;
use strict;
use warnings;
use AnyEvent;
use Linux::Inotify2;
use Carp 'croak';
use Fcntl qw(SEEK_SET);
use Exporter 'import';

use constant {
    ERR_OFFSET_TOO_BIG => 1,
};

our @EXPORT_OK = qw(
    ERR_OFFSET_TOO_BIG
);

sub new {
    my ($class, $filename) = @_;
    croak  'no filename' unless defined $filename;

    my $self = {
        filename     => $filename,
        filesize     => 0,
        update_queue => [],
    };
    bless $self => $class;

    $self->_watch_file;
    $self->_open_file;
    $self->_update_stat;

    return $self;
}

sub add_reader {
    my ($self, $offset, $cb) = @_;

    if ($offset > $self->{filesize}) {
        return $cb->(ERR_OFFSET_TOO_BIG);
    }

    my $reader = {
        offset => $offset,
        cb     => $cb,
    };
    $self->_read_file($reader);
    push @{$self->{update_queue}}, $reader
        unless $reader->{closed};
    return $reader;
}

sub _read_file {
    my ($self, $reader) = @_;
    while ($reader->{offset} < $self->{filesize}) {
        my $chunk = eval {
            $self->_read_chunk($reader->{offset});
        };
        my $err = $@ if $@;
        $err = $self->{error} if $self->{error};
        $reader->{cb}->($err, $chunk);
        last if $reader->{closed};
        $reader->{offset} += length($chunk);
    }
}

sub _read_chunk {
    my ($self, $offset) = @_;
    my $fh = $self->{fh};
    sysseek($fh, $offset, SEEK_SET) or die $!;
    sysread($fh, my $buf, 4096) // die $!;
    return $buf;
}


sub _on_error {
    my ($self, $error) = @_;
    $self->{error} = $error;
    for my $r (@{$self->{update_queue}}) {
        $r->{cb}->($error);
    }
}

sub _on_modify {
    my ($self) = @_;
    $self->_update_stat;
    for my $reader (@{$self->{update_queue}}) {
        $self->_read_file($reader)
            unless $reader->{closed};
    }
    @{$self->{update_queue}} = grep !$_->{closed}, @{$self->{update_queue}};
}

sub _watch_file {
    my ($self) = @_;

    my $inotify = Linux::Inotify2->new
        // die "Failed to create inotify object : $!\n";

    my $flags = IN_UNMOUNT | IN_IGNORED | IN_MODIFY | IN_Q_OVERFLOW;
    $inotify->watch($self->{filename}, $flags, sub {
        my $e = shift;
        if ($e->IN_IGNORED) {
            $self->_on_error('File was deleted');
            # $e->w->cancel;
        }
        elsif ($e->IN_UNMOUNT) {
            $self->_on_error('Filesystem was unmounted');
            # $e->w->cancel;
        }
        elsif ($e->IN_MODIFY || $e->IN_Q_OVERFLOW) {
            $self->_on_modify;
        }
        else {
            $self->_on_error('Unknown event');
            # $e->w->cancel;
        }
    }) or die $!;

    my $inotify_w = AnyEvent->io(fh => $inotify->fileno, poll => 'r', cb => sub {
        $inotify->poll;
    });
    $self->{inotify_w} = $inotify_w;
}

sub _open_file {
    my ($self) = @_;
    sysopen my $fh, $self->{filename}, Fcntl::O_RDONLY or die $!;
    $self->{fh} = $fh;
}

sub _update_stat {
    my ($self) = @_;
    $self->{filesize} = (stat $self->{fh})[7];
}

1;
