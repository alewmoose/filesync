package Test::FileSync;
use strict;
use warnings;
use File::Temp;
use Digest::MD5;
use File::Spec ();
use Cwd();
use lib File::Spec->catdir(__FILE__, File::Spec->updir, 'lib');
use Test::Process;

sub get_dir {
    my (undef, $test_dir) = File::Spec->splitpath(__FILE__);
    my $updir = File::Spec->updir;
    my $dir = File::Spec->catdir(__FILE__, ($updir) x 3);
    Cwd::abs_path($dir);
}

my $dir = get_dir() or die 'no dir';

my $server_script_fn = File::Spec->catdir($dir, 'server.pl');
my $client_script_fn = File::Spec->catdir($dir, 'client.pl');

my $port = 8888;
my $localhost = '127.0.0.1';

sub new {
    my ($class) = @_;
    return bless {
        server  => undef,
        clients => [],
    } => $class;
}

sub start_server {
    my ($self) = @_;
    my $file = $self->server_file;
    my $server = Test::Process->new(
        'perl',
        $server_script_fn,
        "--file=$file",
        "--port=$port",
    );
    $server->is_alive or die "server is not alive";
    $self->{server} = {
        process => $server,
        file    => $file,
    };
}

sub server_file {
    my ($self) = @_;
    File::Spec->catdir($self->tmp_dir, 'server_file');
}

sub create_server_file {
    my ($self) = @_;
    my $file = $self->server_file;
    open my $fh, '>', $file or die "$file: $!";
}

sub append_rand_bytes_to_server_file {
    my ($self, $len) = @_;
    my $file = $self->server_file;
    open my $fh, '>>:raw:bytes', $file or die "$file: $!";
    my $bytes = _rand_bytes($len);
    $fh->print($bytes);
}

sub start_client {
    my ($self, $fn) = @_;
    my $client_no = @{$self->{clients}} + 1;
    $fn //= "client_file_$client_no";
    my $filename = File::Spec->catdir($self->tmp_dir, $fn);
    my $client = Test::Process->new(
        'perl',
        $client_script_fn,
        "--file=$filename",
        "--host=$localhost",
        "--port=$port",
    );
    $client->is_alive or die "client is not alive";
    push @{$self->{clients}}, {
        process => $client,
        file    => $filename,
    };
}

sub start_clients {
    my ($self, $n) = @_;
    $self->start_client for 1..$n;
}

sub server_alive {
    my ($self) = @_;
    $self->{server}{process}->is_alive;
}

sub all_clients_alive {
    my ($self) = @_;
    for my $client (@{$self->{clients}}) {
        return 0 unless $client->{process}->is_alive;
    }
    return 1;
}

sub all_processes_alive {
    my ($self) = @_;
    $self->server_alive && $self->all_clients_alive;
}

sub kill_server {
    my ($self) = @_;
    $self->{server}{process}->kill;
}

sub kill_all_clients {
    my ($self) = @_;
    $_->{process}->kill for @{$self->{clients}};
}

sub all_files_eq {
    my ($self) = @_;
    my ($prev_length, $prev_md5_sum);
    for my $p ($self->{server}, @{$self->{clients}}) {
        my $filename = $p->{file};
        my $fh = eval { _open_file($filename) };
        if ($@) {
            warn $@;
            return 0;
        }
        my $length  = _file_length($fh);
        my $md5_sum = _file_md5_sum($fh);

        $prev_length  //= $length;
        $prev_md5_sum //= $md5_sum;
        return 0 if $length != $prev_length;
        return 0 if $md5_sum ne $prev_md5_sum;
    }
    return 1;
}

sub tmp_dir {
    my ($self) = @_;
    $self->{tmp_dir} //= File::Temp::tempdir(
        'filesync-test-XXXXX',
        TMPDIR => 1,
        CLEANUP => 1,
    );
    $self->{tmp_dir};
}

sub _open_file {
    my ($filename) = @_;
    open my $fh, '<:raw:bytes', $filename or die "$filename: $!";
    $fh;
}

sub _file_md5_sum {
    my ($fh) = @_;
    Digest::MD5->new->addfile($fh)->hexdigest;
}

sub _file_length {
    my ($fh) = @_;
    (stat $fh)[7];
}

sub _rand_bytes {
    my ($len) = @_;
    return join '', map chr(rand 256), 1..$len;
}

1;
