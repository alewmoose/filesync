package Test::Process;
use strict;
use warnings;
use POSIX ":sys_wait_h";

sub new {
    my ($class, $cmd, @args) = @_;
    my $pid = fork // die 'Failed to fork';
    if ($pid == 0) {
        exec $cmd, @args or die "exec: $!";
    }
    return bless {
        cmd  => $cmd,
        args => \@args,
        pid  => $pid,
    } => $class;
}

sub is_alive {
    my ($self) = @_;
    for (1..10) {
        return 0 if waitpid($self->{pid}, WNOHANG) < 0;
    }
    return 1;
}

sub kill {
    my ($self) = @_;
    kill 'TERM', $self->{pid} or die "kill: $!";
}

1;
