use strict;
use warnings;
use Test::More;
use File::Spec ();
use Time::HiRes qw(sleep);
use lib File::Spec->catdir(
    (File::Spec->splitpath(__FILE__))[1], File::Spec->updir, 'lib'
);
use Test::FileSync;

{
    my $t = Test::FileSync->new;
    $t->start_server;
    $t->start_client;
    sleep 1;
    ok !$t->server_alive, 'server exits (no file)';
    ok $t->all_clients_alive, 'clients trying to connect';
    $t->kill_all_clients;
}

{
    my $t = Test::FileSync->new;
    $t->create_server_file;
    $t->append_rand_bytes_to_server_file(100);
    $t->start_server;

    $t->start_client('client_file');
    sleep 1;
    ok $t->all_clients_alive, 'started the first client';
    sleep 5;
    ok $t->all_files_eq, 'all files synced';

    eval { $t->start_client('client_file') };
    sleep 1;
    ok !$t->{clients}[-1]{process}->is_alive,
        'cannot start another client with the same file';
    pop @{$t->{clients}};

    $t->append_rand_bytes_to_server_file(100);
    sleep 2;
    ok $t->all_files_eq, 'all files synced';

    $t->kill_all_clients;
    $t->kill_server;
}

{
    my $t = Test::FileSync->new;
    $t->create_server_file;
    $t->append_rand_bytes_to_server_file(10_000);
    $t->start_server;
    $t->start_clients(5);
    sleep 5;
    ok $t->server_alive, 'server is alive';
    ok $t->all_clients_alive, 'all clients are alive';
    $t->kill_server;
    $t->kill_all_clients;
    ok $t->all_files_eq, 'all files synced';
}

{
    my $t = Test::FileSync->new;
    $t->create_server_file;
    sleep 0.5;
    $t->start_server;
    $t->start_clients(10);

    sleep 5;
    ok $t->all_files_eq, 'clients created empty files';

    for (1..10) {
        $t->append_rand_bytes_to_server_file(100);
        sleep 5;
        ok $t->all_files_eq, 'all files synced';
    }

    $t->kill_all_clients;
    $t->kill_server;
    ok $t->all_files_eq, 'all files synced';
}

{
    my $t = Test::FileSync->new;
    $t->create_server_file;

    sleep 0.5;
    $t->start_server;
    $t->start_clients(10);

    for (1..1_000_000) {
        $t->append_rand_bytes_to_server_file(rand(10)+1);
    }
    sleep 3;

    ok $t->server_alive, 'server is alive';
    ok $t->all_clients_alive, 'all clients are alive';

    $t->kill_all_clients;
    $t->kill_server;
    ok $t->all_files_eq, 'all files synced';
}

{
    my $t = Test::FileSync->new;
    $t->create_server_file;

    sleep 0.5;
    $t->start_server;
    $t->start_clients(10);

    for (1..10) {
        $t->append_rand_bytes_to_server_file(1_000_000+rand(10_000));
    }
    sleep 10;

    ok $t->server_alive, 'server is alive';
    ok $t->all_clients_alive, 'all clients are alive';

    $t->kill_all_clients;
    $t->kill_server;
    ok $t->all_files_eq, 'all files synced';
}

done_testing;

