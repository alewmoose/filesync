#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use Fcntl qw(:flock SEEK_END O_CREAT O_APPEND O_WRONLY);
use AnyEvent;
use AnyEvent::Handle;
use AnyEvent::Socket;
use File::Spec;
use FindBin;
use lib File::Spec->catdir($FindBin::Bin, 'lib');
use FileSync::Protocol qw(
    pack_message
    unpack_header
    unpack_chunk
    HEADER_SIZE
    BEGIN_T
    CHUNK_T
    ERR_OFFSET_TOO_BIG_T
);

my $usage = "Usage: $0 --host=<host> --port=<port> --file=<file>";

my ($host, $port, $file);
GetOptions('host=s' => \$host, 'port=s' => \$port, 'file=s' => \$file);
for ($host, $port, $file) {
    defined or die "$usage\n";
}

my $fh = open_file($file);
my $retry_interval_sec = 3;

while (1) {
    my $file_size = get_file_size($fh);
    my $err_cv = AnyEvent->condvar;
    my $h; $h = AnyEvent::Handle->new(
        connect => [ $host, $port ],
        on_error => sub {
            my ($handle, $fatal, $msg) = @_;
            $handle->destroy;
            $err_cv->send({ error => $msg });
        },
    );

    my $msg = pack_message(type => BEGIN_T, size => $file_size);
    $h->push_write($msg);

    my $fatal_err;
    receive_and_save_chunks($h, $fh, sub {
        my ($err) = @_;
        $err_cv->send({ error => $err, fatal => 1 });
    });

    my $err = $err_cv->recv;
    die "$err->{error}\n" if $err->{fatal};

    warn "$err->{error}, retry in $retry_interval_sec sec\n";
    sleep $retry_interval_sec;
}

sub receive_and_save_chunks {
    my ($h, $fh, $err_cb) = @_;

    $h->push_read(chunk => 5, sub {
        my $header = eval { unpack_header($_[1]) };
        if (my $err = $@) {
            return $err_cb->($err);
        }
        if ($header->{type} == CHUNK_T) {
            $h->push_read(chunk => $header->{length}, sub {
                my $chunk = eval { unpack_chunk($_[1]) };
                if (my $err = $@) {
                    return $err_cb->($err);
                }
                syswrite $fh, $chunk;
            });
            @_ = ($h, $fh);
            goto &receive_and_save_chunks;
        }
        elsif ($header->{type} == ERR_OFFSET_TOO_BIG_T) {
            return $err_cb->("Error: file is too big");
        }
        else {
            return $err_cb->("unknown error");
        }
    });
}

sub open_file {
    my ($file) = @_;
    sysopen my $fh, $file, O_CREAT | O_APPEND | O_WRONLY or die "sysopen: $!";
    flock $fh, LOCK_EX | LOCK_NB or die "flock: $!";
    return $fh;
}

sub get_file_size {
    my ($fh) = @_;
    return (stat($fh))[7];
}

