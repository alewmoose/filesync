#!/usr/bin/env perl
use warnings;
use FindBin;
use lib File::Spec->catdir($FindBin::Bin, 'lib');
use Benchmark::FileSync;

my $mb = 1024 * 1024;

my @benchmarks = (
    {
        initial_file_size => 0,
        nr_clients_synced => 0,
        nr_clients_fresh  => 1,
        append_chunk_size => 100,
        nr_appends        => 1000,
        repeat            => 5,
    },
    {
        initial_file_size => 0,
        nr_clients_synced => 0,
        nr_clients_fresh  => 100,
        append_chunk_size => 100,
        nr_appends        => 1000,
        repeat            => 5,
    },
    {
        initial_file_size => 100 * $mb,
        nr_clients_synced => 0,
        nr_clients_fresh  => 1,
        append_chunk_size => 0,
        nr_appends        => 0,
        repeat            => 1,
    },
    {
        initial_file_size => 10 * $mb,
        nr_clients_synced => 10,
        nr_clients_fresh  => 5,
        append_chunk_size => 100,
        nr_appends        => 10000,
        repeat            => 1,
    },
);

for my $p (@benchmarks) {
    my $b = Benchmark::FileSync->new(%$p);
    $b->run;
    $b->pretty_print_stats;
}
