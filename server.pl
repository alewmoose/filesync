#!/usr/bin/env perl
use strict;
use warnings;
use AnyEvent;
use AnyEvent::Socket;
use AnyEvent::Handle;
use Getopt::Long;
use File::Spec;
use JSON::XS;
use FindBin;
use lib File::Spec->catdir($FindBin::Bin, 'lib');
use FileSync::File qw(ERR_OFFSET_TOO_BIG);
use FileSync::Protocol qw(
    unpack_header
    pack_message
    BEGIN_T
    CHUNK_T
    ERR_OFFSET_TOO_BIG_T
    ERR_UNKNOWN_T
);

my $usage = "Usage: $0 --port=<port> --file=<file> [--stats-port=<port>]";
my $default_stats_port = 9000;

my ($port, $filename, $stats_port);
GetOptions(
    'port=s'       => \$port,
    'file=s'       => \$filename,
    'stats-port=s' => \&stats_port,
);
for ($port, $filename) {
    defined or die "$usage\n";
}
$stats_port //= $default_stats_port;
if ($port == $stats_port) {
    die "port = stats-port\n";
}

my $file = FileSync::File->new($filename);
my @readers;
my $nr_closed_readers = 0;

tcp_server(undef, $port, sub {
    my ($fh, $host, $port) = @_;
    my $h = AnyEvent::Handle->new(fh => $fh);
    my $reader;
    my $close_reader = sub {
        $nr_closed_readers++;
        if ($reader) {
            $reader->{closed} = 1;
            @readers = grep !$_->{closed}, @readers;
        }
        undef $h;
    };
    $h->on_error($close_reader);
    $h->on_eof($close_reader);
    $h->push_read(chunk => 5, sub {
        my $header = eval { unpack_header($_[1]); };
        if (my $err = $@) {
            my $msg = pack_message(type => ERR_UNKNOWN_T, size => 0);
            return $h->push_write($msg);
            $close_reader->();
        }

        my $offset;
        if ($header->{type} == BEGIN_T) {
            $offset = $header->{length};
        }
        else {
            my $msg = pack_message(type => ERR_UNKNOWN_T, size => 0);
            return $h->push_write($msg);
            $close_reader->();
        }

        $reader = $file->add_reader($offset, sub {
            my ($err, $chunk) = @_;
            if ($err) {
                my $type;
                if ($err == ERR_OFFSET_TOO_BIG) {
                    $type = ERR_OFFSET_TOO_BIG_T
                }
                else {
                    $type = ERR_UNKNOWN_T;
                }
                my $msg = pack_message(type => $type, size => 0);
                $h->push_write($msg);
                $close_reader->();
                return;
            }
            my $msg = pack_message(
                type  => CHUNK_T,
                size  => length($chunk),
                chunk => $chunk,
            );
            $h->push_write($msg);
        });
        push @readers, $reader;
    });
});

tcp_server(undef, $stats_port, sub {
    my ($fh, $host, $port) = @_;
    my $h = AnyEvent::Handle->new(fh => $fh);
    my $close = sub { undef $h };
    $h->on_error($close);
    serve_stats($h, $close);
});

sub serve_stats {
    my ($h, $cb) = @_;
    $h->push_read(json => sub {
        my $query = eval { $_[1]->{query} } // '';
        if ($query eq 'get_stats') {
            my $clients = [ map { { offset => $_->{offset} } } @readers ];
            $h->push_write(json => {
                filename          => $file->{filename},
                filesize          => $file->{filesize},
                clients           => $clients,
                nr_closed_clients => $nr_closed_readers,
            });
        }
        elsif ($query eq 'close') {
            goto &$cb;
        }
        else {
            $h->push_write(json => { error => 'invalid query' });
        }
        @_ = ($h, $cb);
        goto &serve_stats;
    });
}

AnyEvent->condvar->recv;
